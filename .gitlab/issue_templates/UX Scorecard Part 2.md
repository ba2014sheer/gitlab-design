<!--

# The issue title should be: {{YYYY}}{{Quarter}} Part 2: Experience Recommendations for {{Stage Group}} - {{JTBD}} 

-->

- **UX Scorecard issue**: {{add link to UX scorecard issue}}

## Experience Recommendations Checklist

[Learn more about UX Scorecards](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/)

1. [ ] Brainstorm opportunities to fix or improve areas of the experience.
   - Use the findings from the Emotional Grading scale to determine areas of immediate focus. For example, if parts of the experience received a “Negative” Emotional Grade, consider addressing those first. 
1. [ ] Create an issue for each recommendation and add them as related to this issue. 
1. [ ] Think iteratively, and create dependencies where appropriate, remembering that sometimes the order of what we release is just as important as what we release.
   - If you need to break recommendations into phases or over multiple milestones, create multiple epics and use the [Category Maturity Definitions](https://about.gitlab.com/direction/maturity/) in the title of each epic: **Minimal, Viable, Complete, or Lovable**.

/label ~"UX scorecard" 
